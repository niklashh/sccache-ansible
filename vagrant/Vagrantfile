def provisioned?(vm_name='default', provider='virtualbox')
  File.exist?(".vagrant/machines/#{vm_name}/#{provider}/action_provision")
end

Vagrant.configure("2") do |config|
  config.ssh.insert_key = false

  config.vm.define "sccache" do |sccache|
    sccache.vm.provider :virtualbox do |vb|
      vb.memory = 8192
      vb.cpus = 6
      vb.name = "sccache"
    end

    config.vm.box = "debian/bullseye64"
    sccache.vm.define "sccache"
    sccache.vm.hostname = "sccache"
    # sccache.vm.network :forwarded_port, id: "ssh", guest: 22, host: 2222, disabled: true
    sccache.vm.network :private_network, ip: "192.168.56.2"
    sccache.vm.synced_folder ".", "/vagrant", disabled: true

    sccache.ssh.insert_key = false
    sccache.ssh.private_key_path = ['~/.vagrant.d/insecure_private_key', 'id_rsa']

    if not provisioned?("sccache")
      sccache.vm.provision "file", source: "./id_rsa.pub", destination: "~/.ssh/authorized_keys"
    else
      sccache.ssh.host = "192.168.56.2"
      sccache.ssh.port = 22
    end

    sccache.vm.provision :ansible do |ansible|
      ansible.playbook = "../host.yml"
      ansible.config_file = "../ansible.cfg"
      ansible.inventory_path = "inventory.yml"
      ansible.extra_vars = {
        "pre_host": "192.168.56.2"
      }
      ansible.compatibility_mode = "2.0"
      ansible.verbose = "vv"
    end
  end
end
